#!/busybox/env sh

export BUILD_INFO_GIT_COMMIT=$(cat /build-info/git-commit)
export BUILD_INFO_BUILD_TIME=$(cat /build-info/finished-at)

exec $@
