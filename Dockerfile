# syntax=docker/dockerfile:1.0.0-experimental

# Build stage
#FROM golang:1.17 as builder

#RUN mkdir -p -m 0600 ~/.ssh \
#    && ssh-keyscan gitlab-site >> ~/.ssh/known_hosts

## Build go binary
#RUN mkdir /workdir && cd /workdir
#WORKDIR /workdir

#COPY go.* .
#RUN go mod download

#COPY . .
#RUN mkdir /build-info \
#    && git rev-parse --short HEAD >> /build-info/git-commit \
#    && date >> /build-info/started-at \
#    && go build -o app . \
#    && date >> /build-info/finished-at

# Final stage
#FROM gcr.io/distroless/base:debug

#COPY --from=builder /workdir/app /app
#COPY --from=builder /build-info /build-info
#COPY --from=builder /workdir/entrypoint.sh /entrypoint.sh

#ENTRYPOINT ["/entrypoint.sh"]
#CMD ["/app"]
